//Compile by adding -lm to the end of command or you get a "undefined reference to `pow'" error

#include <stdio.h>

// stb
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"


int string_equals (char arg[], const char string[], int string_size) {
   // string_equals (argv value, custom string, custom string length) - (argv[1], "info", 4)
   for (int ix = 0; ix < string_size; ix++) {
      if (arg[ix] != string[ix]) {
         return 0;
      }
   }
   if (arg[string_size] != '\0') {
      return 0;
   }
   return 1;
}

void print_info() {
   printf("############     IMG TO GBDK HEX ARRAY     ############\n            ###############################\n\nUses stb_image.h to decode an image to greyscale and then\nconverts the pixel data into a 'GameBoy Developer Kit'-\ncompatible format (4 colors, 2 bytes per pixel)\n\nUSAGE:\n\t((  program_name  image_file  variable_name  ))\n\t[  img2GB flower.png flower_tiles  ]\n\t[  img2GB info  ]  or  [  img2GB help  ]\n\t[  img2GB flower.png flower_tiles >> my_file.c  ]\n\nInput image:\n\ttype:\tall supported by stb_image.h (only PNG tested)\n\t\t\t(JPEG PNG TGA BMP PSD GIF* HDR PIC PNM)\n\twidth:\tmultiple of 8 (8, 16, 32, 256...)\n\theight:\tmultiple of 8 (8, 16, 32, 256...)\n\tformat:\t8x8 px tiles, read left->right top->bottom\nOutput:\n\tGDBK2020-compatible format (const unsigned char[])\n\tarray with hex pairs that form the 16 bytes of each 8x8\n\ttile. Each pair's bit tuples form a 4 color encoding.\n");
}


int main(int argc, char *argv[]) {

   char *filename;
   char *var_name;

   if (argc == 1) {           // print info
      print_info();
      return 0;               // EXIT
   }
   else if (argc == 2) {      // program name, info?
      if(string_equals(argv[1], "info", 4) || string_equals(argv[1], "help", 4)) {
         print_info();
         return 0;            // EXIT
      }
      else {                  // program name, image file name
         filename = argv[1];
         char default_varname[] = "tiles";
         var_name = default_varname; 
      }
   }
   else if (argc == 3) {      // program name, image file name, variable name
      filename = argv[1];
      var_name = argv[2]; 
   }
   else {
      print_info();
      return 0;               // EXIT
   }

   // ##########  LOAD FILEDATA AS GREYSCALE  ##########
   // ##################################################

   int x,y,n;  // x - image width, y - image height, n - channels (grey(1) ... rgba(4))
   unsigned char *img_data;

   // rgb to greyscale formula is in stbi__compute_y (77, 150, 29)(/256)
   // similar to the Luma coding approximation Y = 0.299(R) + 0.587(G) + 0.114(B)
   img_data = stbi_load(filename, &x, &y, &n, 1);  // (0-auto, 1-grey, 3-rgb, 4-rgba)
   if (img_data == NULL) { 					         // if error while opening
		fprintf(stderr, "(;^_^) Oopsie! Unable to open file -> %s\n", filename); 
		return 0;
	}


   // ##########  CHECK IMAGE DIMENSIONS  ##########
   // ##############################################

   if ((x & 0x07) != 0) {
      fprintf(stderr, "(;^_^) Oopsie! Image width is not a multiple of 8.\n"); 
		return 0;
   }

   if ((y & 0x07) != 0) {
      fprintf(stderr, "(;^_^) Oopsie! Image height is not a multiple of 8.\n"); 
		return 0;
   }


   // ##########  CHECK PIXELS AND MAKE TYPE ARRAY  ##########
   // ########################################################

   int pixel_hues[4];               // stores up to 4 greyscale colors
   int hues_filled = 0;             // keeps track of nr of slots we used (4 available)
   int total_pixels = x * y;        // pixels in image
   int pixel_types[total_pixels];    // categorize img_data colors into 4 possible types
   int i, ii, end_loop;
   
   for (i = 0; i < total_pixels; i++){
      end_loop = 0;
      for (ii = 0; ii < hues_filled; ii++) {
         if (img_data[i] == pixel_hues[ii]) {
            pixel_types[i] = ii;
            end_loop = 1;
            break;
         }
      }
      if (end_loop == 0) {
         if (hues_filled < 4) {
            pixel_hues[hues_filled] = img_data[i];
            pixel_types[i] = hues_filled;
            hues_filled += 1;
         }
         else {                        // if all 4 GB hues are filled, then give error and abort
            fprintf(stderr, "(;^_^) Oopsie! The image has more than 4 colors.\n"); 
            return 0;
         }
      }
   }


   // ##########  GET ORDER OF HUE VALUES  ##########
   // ###############################################

   int hue_order[4] = {0, 0, 0, 0};

   for (i = 0; i < hues_filled; i++) {
      for (ii = 0; ii < hues_filled; ii++) {
         if (pixel_hues[i] < pixel_hues[ii]) {
            hue_order[i] += 1;   // count how many numbers is the number lower than (grey 255-0, GB 0-3)
         }
      }
   }


   // ##########  ADJUST TYPES TO REPRESENT ACTUAL HUES  ##########
   // #############################################################

   for (i = 0; i < total_pixels; i++) {
      pixel_types[i] = hue_order[pixel_types[i]];
   }


   // ##########  CONVERTING PIXEL HUES INTO 2 BITS  ##########
   // #########################################################

   // GB rows are made up of 2 bytes
   // combinations of their specific bits make a 2-bit value which holds the 4 possible hues
   int total_bytes = total_pixels / 8;
   int row_byte_1[total_bytes];
   int row_byte_2[total_bytes];
   int byte_counter = 0;
   int i_counter = 0;

   for (i = 0; i < total_pixels; i += 8) {
      row_byte_1[byte_counter] = 0;
      row_byte_2[byte_counter] = 0;
      for (ii = 0; ii < 8; ii++) {
         i_counter = i + ii;
         row_byte_1[byte_counter] <<= 1;      // shift left by 1 to create new space (a<<=1 == a=a<<1)
         row_byte_2[byte_counter] <<= 1;      
         if (pixel_types[i_counter] == 1) {           // 01
            row_byte_2[byte_counter] |= 1;    // flipping second bit to 1 with bitwise OR and 00000001
         }
         else if (pixel_types[i_counter] == 2) {      // 10
            row_byte_1[byte_counter] |= 1;    // flipping first bit to 1 with bitwise OR and 00000001
         }
         else if (pixel_types[i_counter] == 3) {      // 11
            row_byte_1[byte_counter] |= 1;    // flipping both bits to 1 and shifting them to left by 1
            row_byte_2[byte_counter] |= 1;    
         }                                   // if pixel is 00, then no need to do anything
      }
      byte_counter++;
   }
   

   // ##########  CONSOLIDATE DATA INTO FRAMES  ##########
   // ####################################################

   int h_frames = x / 8;
   int v_frames = y / 8;
   int final_total_bytes = total_bytes * 2;
   int final_data[final_total_bytes];
   int frame_byte, data_index, iii;

for (i = 0; i < v_frames; i++) {
   for (ii = 0; ii < h_frames; ii++) {
      for (iii = 0; iii < 8; iii++) {
         frame_byte = (i * h_frames * 8) + ii + (iii * h_frames);
         data_index = ((i * h_frames * 8) + (ii * 8) + iii) * 2;
         // in a GBDK sprite tutorial, the bytes appear to be switched, that's why we fill 2-1
         final_data[data_index] = row_byte_2[frame_byte];
         final_data[data_index + 1] = row_byte_1[frame_byte];
      }
   }
}

   // ##########  GENERATE HEX OUTPUT  ##########
   // ###########################################

   printf("const unsigned char %s[] = {\n", var_name);
   printf("\t0x%02X", final_data[0]);  // 02 means print 2 digits and if less, write 0 in front
   i_counter = 7;
   for (i = 1; i < final_total_bytes; i++) {
      if (i_counter) {
         printf(",0x%02X", final_data[i]);
      }
      else {
         printf(",\n\t0x%02X", final_data[i]);
         i_counter = 8;
      }
      i_counter--;
   }
   printf("\n};\n");
   

}

