############     IMG TO GBDK HEX ARRAY     ############
            ###############################

Uses stb_image.h to decode an image to greyscale and then
converts the pixel data into a 'GameBoy Developer Kit'-
compatible format (4 colors, 2 bytes per pixel)

USAGE:
	((  program_name  image_file  variable_name  ))
	[  img2GB flower.png flower_tiles  ]
	[  img2GB info  ]  or  [  img2GB help  ]
	[  img2GB flower.png flower_tiles >> my_file.c  ]

Input image:
	type:	all supported by stb_image.h (only PNG tested)
			(JPEG PNG TGA BMP PSD GIF* HDR PIC PNM)
	width:	multiple of 8 (8, 16, 32, 256...)
	height:	multiple of 8 (8, 16, 32, 256...)
	format:	8x8 px tiles, read left->right top->bottom

Output:
	GDBK2020-compatible format (const unsigned char[])
	array with hex pairs that form the 16 bytes of each 8x8
	tile. Each pair's bit tuples form a 4 color encoding.
